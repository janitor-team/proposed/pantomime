Source: pantomime
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders: Eric Heintzmann <heintzmann.eric@free.fr>,
           Yavor Doganov <yavor@gnu.org>
Section: libs
Priority: optional
Build-Depends: debhelper (>= 12),
               gnustep-make (>= 2.7.0-4),
               libgnustep-base-dev
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/gnustep-team/pantomime
Vcs-Git: https://salsa.debian.org/gnustep-team/pantomime.git
Homepage: http://wiki.gnustep.org/index.php/Pantomime

Package: libpantomime1.3
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: GNUstep framework for mail handling (runtime library)
 Pantomime provides a GNUstep framework that models a mail system. Pantomime
 can be seen as a JavaMail 1.2 clone written in Objective-C.
 .
 This package contains the runtime library files.

Package: libpantomime-dev
Architecture: any
Section: libdevel
Depends: libpantomime1.3 (= ${binary:Version}),
         libgnustep-base-dev,
         ${misc:Depends}
Breaks: libpantomime1.2-dev
Replaces: libpantomime1.2-dev
Description: GNUstep framework for mail handling (development files)
 Pantomime provides a GNUstep framework that models a mail system. Pantomime
 can be seen as a JavaMail 1.2 clone written in Objective-C.
 .
 Pantomime provides the following features:
 .
  * A full MIME encoder and decoder
  * A "folder view" to POP3 accounts, local (Berkeley Format) or IMAP mailboxes
  * A powerful API to work on all aspects of Message objects
  * A local mailer and a SMTP conduit for sending messages
  * APOP and SMTP AUTH support
  * IMAP and POP3 URL Scheme support
  * iconv and Core Foundation support
  * maildir support
  * SSL support for IMAP, POP3 and SMTP
  * and more!
 .
 This package contains the files required to develop applications that use
 Pantomime.
